import NotFoundImage from "../../assets/images/404.gif";

const ASSETS_ENUM = {
  NOT_FOUND_IMG: NotFoundImage,
};

export default ASSETS_ENUM;
