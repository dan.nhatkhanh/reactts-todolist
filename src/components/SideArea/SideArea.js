import React from 'react';
import SideBar from './SideBar/SideBar';
import TodoArea from './TodoArea/TodoArea';

function SideArea() {
  return (
    <div className='flex w-96 bg-[#264653] text-slate-100'>
      <SideBar />
      <TodoArea />
    </div>
  );
}

export default SideArea;
