import React from 'react';
import {TodoButton} from '../../../libs';

function UserAvailable() {
  const userList = [
    {id: 1, imageSrc: 'https://i.pravatar.cc/300'},
    {id: 2, imageSrc: 'https://i.pravatar.cc/300'},
    {id: 3, imageSrc: 'https://i.pravatar.cc/300'},
    {id: 4, imageSrc: 'https://i.pravatar.cc/300'},
  ];
  const onRenderUser = () => {
    return userList.map((user) => {
      return (
        <div key={user.id} className='mt-2 px-3'>
          <img
            className='w-full rounded-full'
            src={user.imageSrc}
            alt='user'
            loading='lazy'
          />
        </div>
      );
    });
  };
  return (
    <div className='py-1'>
      {onRenderUser()}
      <button className='mt-2 flex items-center justify-center px-3'>
        <i className='fa-solid fa-plus rounded-full bg-[#508ea7]  p-3'></i>
      </button>
    </div>
  );
}

export default UserAvailable;
