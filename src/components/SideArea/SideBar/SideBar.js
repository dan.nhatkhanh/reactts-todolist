import React from 'react';
import Logo from '../../Logo/Logo';
import UserAvailable from './UserAvailable';

function SideBar() {
  return (
    <div className='flex w-16 flex-col items-center justify-start bg-[#376476] py-10'>
      <Logo />
      <p className='relative my-3 flex h-10 w-10 items-center justify-center rounded-lg bg-[#E9C46A] text-xl font-bold text-[#264653] before:absolute before:top-[5px] before:h-1 before:w-2/5 before:rounded-xl before:bg-[#264653] before:content-[""]'>
        8
      </p>
      <hr className='w-1/2 border-t-2 border-white' />
      <UserAvailable />
    </div>
  );
}

export default SideBar;
