import {TodoInput} from '../../libs';

function TodoCategories() {
  const options = [
    {value: '', text: 'All'},
    {value: 'incomplete', text: 'Incomplete'},
    {value: 'complete', text: 'Complete'},
  ];

  return <TodoInput type='drop-down' options={options} />;
}

export default TodoCategories;
