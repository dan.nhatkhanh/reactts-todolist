import {TodoButton, TodoInput} from '../../libs';

function TodoAdd() {
  return (
    <div className='flex flex-wrap'>
      <TodoInput placeholder='Add task' />
      <TodoButton content='Add' />
    </div>
  );
}

export default TodoAdd;
