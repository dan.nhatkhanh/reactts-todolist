import {TodoInput} from '../../libs';

function TodoSearch() {
  return <TodoInput placeholder='Search...' id='search' name='search' />;
}

export default TodoSearch;
