import TodoCategories from './TodoCategories';
import TodoInput from './TodoAdd';
import TodoSearch from './TodoSearch';

function TodoHeader() {
  return (
    <div className='block'>
      <TodoSearch />
      <div className='flex justify-between py-3'>
        <TodoInput />
        <TodoCategories />
      </div>
    </div>
  );
}

export default TodoHeader;
