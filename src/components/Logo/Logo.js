import React from 'react';

function Logo() {
  return <i className='fa-solid fa-crop-simple text-3xl'></i>;
}

export default Logo;
