// input type
export interface TodoInputProps {
  type: string;
  options: InputDropDownOption;
  inputProps: InputProps;
}

export interface InputProps {
  inputProps: any;
}

export interface InputDropDownOption {
  value: string;
  text: string;
}

export interface InputDropDown {
  options: InputDropDownOption[];
  inputProps: any;
}

// button type
export interface TodoButtonProps {
  type: string;
  content: string;
  icon: string;
  shape: string;
  className: string;
  buttonProps: ButtonProps;
}

export interface ButtonProps {
  buttonProps: any;
}

export interface ButtonVariantCommonProps {
  content: string;
  icon: string;
  shape: string;
  className: string;
  buttonProps: ButtonProps;
}

export interface ButtonVariantTableProps {
  icon: string;
  className: string;
  buttonProps: ButtonProps;
}

export interface HiddenIconProps {
  content: string;
  icon: string;
  shape: string;
}

export interface HiddenContentProps {
  content: string;
  shape: string;
}
