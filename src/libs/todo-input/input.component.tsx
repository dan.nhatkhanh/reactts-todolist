import PropTypes from 'prop-types';
import {ReactElement} from 'react';
import {InputDropDownOption, InputProps, TodoInputProps} from '../types';

const variantDefault = (inputProps: InputProps) => {
  return (
    <input
      className='mr-3 rounded-lg border-2 border-slate-300 px-3 py-1 text-indigo-500 outline-indigo-500'
      type='text'
      {...inputProps}
    />
  );
};

const variantDropDown = (
  options: InputDropDownOption[],
  inputProps: InputProps
) => {
  const onRenderOption = () => {
    if (options && Array.isArray(options)) {
      return options.map((item) => {
        return (
          <option key={item.value} value={item.value}>
            {item.text}
          </option>
        );
      });
    }
    return;
  };
  return (
    <select
      className='cursor-pointer rounded-lg border-2 border-slate-300 px-3 py-1 text-indigo-500 outline-indigo-500'
      {...inputProps}
    >
      {onRenderOption()}
    </select>
  );
};

TodoInputComponent.propTypes = {
  type: PropTypes.oneOf(['default']),
};

TodoInputComponent.defaultProps = {
  type: 'default',
};

function TodoInputComponent(props: TodoInputProps) {
  let {type, options, ...inputProps} = props;
  let res: ReactElement<any, any>;

  const onRender = () => {
    switch (type) {
      case 'default':
        res = variantDefault(inputProps);
        break;

      case 'drop-down':
        if (Array.isArray(options)) {
          res = variantDropDown(options, inputProps);
        }
        break;

      default:
        break;
    }
    return res;
  };

  return onRender();
}

export default TodoInputComponent;
