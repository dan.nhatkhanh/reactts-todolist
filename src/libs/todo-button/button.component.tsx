import PropTypes from 'prop-types';
import {ReactElement} from 'react';
import {
  ButtonVariantCommonProps,
  ButtonVariantTableProps,
  HiddenContentProps,
  HiddenIconProps,
  TodoButtonProps,
} from '../types';

const transition = 'transition duration-300';

const bgPrimary = 'bg-indigo-500 hover:bg-white';
const textPrimary = 'text-lg font-medium text-white hover:text-indigo-500';
const borderPrimary = 'border-2 border-transparent focus:border-indigo-300';
const shadowPrimary = 'hover:shadow-md hover:shadow-indigo-500';

const bgSmooth = 'bg-indigo-100 hover:bg-white';
const textSmooth = 'text-lg font-medium text-indigo-500';
const borderSmooth = 'border-2 border-transparent focus:border-indigo-300';
const shadowSmooth = 'hover:shadow-md hover:shadow-indigo-500';

const bgGhost = 'bg-white hover:bg-indigo-500';
const textGhost = 'text-lg font-medium text-indigo-500 hover:text-white';
const borderGhost =
  'border-[1px] border-indigo-500 hover:border-transparent focus:border-indigo-300';
const shadowGhost = 'hover:shadow-md hover:shadow-indigo-300';

const bgRaised = 'bg-white hover:bg-indigo-500';
const textRaised = 'text-lg font-medium text-indigo-500 hover:text-white';
const borderRaised = 'border-2 border-transparent focus:border-indigo-300';
const shadowRaised = 'shadow-md shadow-indigo-500';

const shapeDefault = 'px-6 py-1 rounded-md';
const shapeCircle = 'w-14 h-14 rounded-full';
const shapeSquare = 'w-14 h-14 rounded-lg';
const shapeAction = 'px-5 py-2 rounded-full';

const iconStyle = 'text-xl';

const handleChangeShape = (shape: string) => {
  switch (shape) {
    case 'default':
      return shapeDefault;
    case 'circle':
      return shapeCircle;
    case 'square':
      return shapeSquare;
    case 'action':
      return shapeAction;

    default:
      return shapeDefault;
  }
};

const handleIconContentDistance = (props: HiddenIconProps) => {
  let {icon, content, shape} = props;
  return icon && content && shape !== 'circle' && shape !== 'square'
    ? 'mr-3'
    : '';
};

const handleHiddenContent = (props: HiddenContentProps) => {
  let {shape, content} = props;
  return shape === 'circle' || shape === 'square' ? '' : content;
};

const handleHiddenIcon = (props: HiddenIconProps) => {
  let {content, icon, shape} = props;
  return (
    shape !== 'default' && (
      <i
        className={`${icon} ${iconStyle} ${handleIconContentDistance(props)}`}
      ></i>
    )
  );
};

const variantDefault = (props: ButtonVariantCommonProps) => {
  const {content, icon, shape, className, buttonProps} = props;
  const hiddenIconProps = {content, icon, shape};
  const hiddenContentProps = {shape, content};
  return (
    <button
      className={`${className} ${bgPrimary} ${textPrimary} ${borderPrimary} ${handleChangeShape(
        shape
      )} ${shadowPrimary} ${transition}`}
      {...buttonProps}
    >
      {handleHiddenIcon(hiddenIconProps)}{' '}
      {handleHiddenContent(hiddenContentProps)}
    </button>
  );
};

const variantSmooth = (props: ButtonVariantCommonProps) => {
  const {content, icon, shape, className, buttonProps} = props;
  const hiddenIconProps = {content, icon, shape};
  const hiddenContentProps = {shape, content};
  return (
    <>
      <button
        className={`${className} ${bgSmooth} ${textSmooth} ${borderSmooth} ${handleChangeShape(
          shape
        )}  ${shadowSmooth} ${transition}`}
        {...buttonProps}
      >
        {handleHiddenIcon(hiddenIconProps)}{' '}
        {handleHiddenContent(hiddenContentProps)}
      </button>
    </>
  );
};

const variantGhost = (props: ButtonVariantCommonProps) => {
  const {content, icon, shape, className, buttonProps} = props;
  const hiddenIconProps = {content, icon, shape};
  const hiddenContentProps = {shape, content};
  return (
    <>
      <button
        className={`${className} ${bgGhost} ${textGhost} ${borderGhost} ${handleChangeShape(
          shape
        )}  ${shadowGhost} ${transition}`}
        {...buttonProps}
      >
        {handleHiddenIcon(hiddenIconProps)}{' '}
        {handleHiddenContent(hiddenContentProps)}
      </button>
    </>
  );
};

const variantRaised = (props: ButtonVariantCommonProps) => {
  const {content, icon, shape, className, buttonProps} = props;
  const hiddenIconProps = {content, icon, shape};
  const hiddenContentProps = {shape, content};
  return (
    <>
      <button
        className={`${className} ${bgRaised} ${textRaised} ${borderRaised} ${handleChangeShape(
          shape
        )}  ${shadowRaised} ${transition}`}
        {...buttonProps}
      >
        {handleHiddenIcon(hiddenIconProps)}{' '}
        {handleHiddenContent(hiddenContentProps)}
      </button>
    </>
  );
};

const variantBtnTable = (props: ButtonVariantTableProps) => {
  let {icon, className, buttonProps} = props;
  return (
    <>
      <button
        className={`${className} text-base font-normal hover:text-indigo-400`}
        {...buttonProps}
      >
        <i className={icon}></i>
      </button>
    </>
  );
};

TodoButtonComponent.propTypes = {
  type: PropTypes.oneOf(['default', 'smooth', 'ghost', 'raised', 'table']),
  content: PropTypes.string,
  icon: PropTypes.string,
  shape: PropTypes.oneOf(['default', 'circle', 'square', 'action']),
};

TodoButtonComponent.defaultProps = {
  type: 'default',
  content: 'button',
  icon: 'fa-solid fa-plus',
  shape: 'default',
};

function TodoButtonComponent(props: TodoButtonProps) {
  let {type, content, icon, shape, className, ...buttonProps} = props;
  let commonProps = {content, icon, shape, className, buttonProps};
  let tableProps = {icon, className, buttonProps};

  let res: ReactElement<any, any>;
  const onRender = () => {
    switch (type) {
      case 'default':
        res = variantDefault(commonProps);
        break;

      case 'smooth':
        res = variantSmooth(commonProps);
        break;

      case 'ghost':
        res = variantGhost(commonProps);
        break;

      case 'raised':
        res = variantRaised(commonProps);
        break;

      case 'table':
        res = variantBtnTable(tableProps);
        break;

      default:
        break;
    }

    return res;
  };

  return <div>{onRender()}</div>;
}

export default TodoButtonComponent;
