import NotFound from "./NotFound/NotFound";
import HomePage from "./Home/HomePage";

const routes = [
  { path: "*", exact: true, public: true, component: <NotFound />, route: [] },
  { path: "/", exact: true, public: true, component: <HomePage />, route: [] },
  {
    path: "/home",
    exact: true,
    public: true,
    component: <HomePage />,
    route: [],
  },
];

export default routes;
