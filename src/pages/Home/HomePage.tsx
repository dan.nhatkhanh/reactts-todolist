import MainArea from '../../components/MainArea/MainArea';
import SideArea from '../../components/SideArea/SideArea';

function HomePage() {
  console.log('Homepage neeeeee');
  return (
    <div className='h-screen bg-white'>
      <div className='flex h-full'>
        <SideArea />
        <MainArea />
      </div>
    </div>
  );
}

export default HomePage;
